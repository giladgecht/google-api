"""

    __author__ : Gilad Gecht
    __date__   : 06/05/2020


"""
# Imports
import sys
import time
import json
import requests
import datetime
import pandas as pd
import concurrent.futures

from read_data import read_s3_bucket_data
from clean_data import transform

def save_checkpoint(out):

    print("Saving...")
    records = []

    for i, record in enumerate(out):
        try:
            r = json.loads(record[0].content)['results']['organic_results']
            for item in r:

                item['searchterm'] = record[1]
            records.append(r)

        except Exception as e:
            print(e, i)


    records = [item for sublist in records for item in sublist]
    records = pd.DataFrame(records)
    records['rank'] = records.groupby(['searchterm']).cumcount()
    records['timestamp'] = datetime.datetime.now()

    try:
        records.to_csv(f's3://mac-enrichement.avantis.cloud/scraped/{datetime.datetime.today()}.csv', index=False)
        print("Data Successfuly Saved to Bucket...")
    except Exception as e:
        print(e)


def load_url(searchterm, timeout=5):
    # for debugging
    # print(searchterm)
    url = "https://google-search5.p.rapidapi.com/get-results"

    querystring = {"country": "us", "limit": "10", "hl": "en-US", "q": searchterm}

    headers = {
        'x-rapidapi-host': "google-search5.p.rapidapi.com",
        'x-rapidapi-key': "4d007094b3msh92cd5b935042201p1d5d3ejsnad599c55ece6"
    }
    response = requests.request("GET", url, headers=headers, params=querystring)
    return response, searchterm


if __name__ == "__main__":

    bucket      = "mac-enrichement.avantis.cloud"
    filename    = "redirects_2020-05-07.csv"     #"unscraped_yet.csv"
    path        = "s3://{}/raw/{}".format(bucket, filename)
    out         = []
    CONNECTIONS = 10
    TIMEOUT     = 5
    t1          = time.time()


    try:
        print("Fetching data from bucket...")
        sentences = pd.read_csv(path)
        print("Data loaded succesffuly...")
    except:
        print("Failed to retrieve data...\nExiting...")
        sys.exit()

    # Processing data
    print("Processing data and removing duplicates...")
    sentences = transform(sentences)
    print("Processing finished...")

    # Filter scraped searchterms
    print("Removing scraped searchterms...")
    scraped_searchterms = read_s3_bucket_data(bucket, 'scraped')
    sentences = list(sentences.difference(scraped_searchterms))


    print("Starting scraper...")
    with concurrent.futures.ThreadPoolExecutor(max_workers=CONNECTIONS) as executor:
        START = 0
        JUMP  = 1000
        idx   = 0

        future_to_url = (executor.submit(load_url, st, TIMEOUT) for st in sentences)
        time1 = time.time()
        for future in concurrent.futures.as_completed(future_to_url):
            try:
                data = future.result()
                if len(out) % 100 == 0 and len(out) != 0:
                    print("Done with: {}".format(len(out)))
                if len(out) % JUMP == 0 and len(out) != 0:
                    save_checkpoint(out[START:START + JUMP])
                    START += JUMP
            except Exception as exc:
                data = str(type(exc))
            finally:
                out.append(data)

        save_checkpoint(out[START:START+JUMP])
        print(f'Took {time.time() - time1:.2f} s')
        time2 = time.time()

    print(f'Took {time2 - time1:.2f} s')
