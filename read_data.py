import os
import boto3
import pandas as pd


def read_s3_bucket_data(bucket_name, sub_directory):
    """

    This method reads an s3 bucket with a destined folder and returns a set of the scraped searchterms

    Params
    ======
    :bucket_name - String
    The name of the bucket

    :sub_directory - String
    The name of the directory/folder the desired files are in


    :return searchterms - Set
    a set of all the scraped searchterms

    """
    bucket = boto3.resource("s3").Bucket(bucket_name)
    searchterms = set()
    for obj in bucket.objects.filter(Prefix=sub_directory):
        file_key = obj.key
        if file_key.endswith(".csv"):
           searchterms = searchterms.union(set(pd.read_csv(obj.get()['Body'])['searchterm'].unique()))

    return searchterms

