import re
import pandas as pd

"""

    __author__ : Gilad Gecht
    __date__   : 07/05/2020

    This file takes a pandas dataframe as a parameter,
    transforms it doing the following:

    - Removes stopwords
    - Removes punctutation and special characters
    - Removes multiple whitespaces

    Eventually returns a set of the distinct searchterms


"""


stop_words = ['of',
 "didn't",
 'd',
 'theirs',
 'more',
 'himself',
 'off',
 'other',
 'once',
 'she',
 'why',
 'any',
 'so',
 'you',
 'am',
 'aren',
 'because',
 'should',
 'under',
 'both',
 "hadn't",
 'those',
 'while',
 'that',
 'needn',
 'isn',
 'about',
 'themselves',
 'yourselves',
 'in',
 'only',
 'doesn',
 "you'll",
 'weren',
 'above',
 'who',
 'have',
 "needn't",
 'shan',
 'an',
 "she's",
 'being',
 'don',
 "weren't",
 'her',
 'but',
 'further',
 'few',
 'yours',
 'ain',
 "you'd",
 'i',
 'here',
 'own',
 'most',
 'if',
 'during',
 'each',
 'is',
 'y',
 'up',
 'all',
 'down',
 'whom',
 'against',
 'm',
 'them',
 'wasn',
 'where',
 're',
 'to',
 "doesn't",
 "shan't",
 'which',
 'these',
 'and',
 'his',
 'haven',
 'too',
 'hadn',
 "you're",
 'with',
 'such',
 'or',
 'no',
 'mustn',
 'herself',
 'just',
 'my',
 'then',
 'having',
 'out',
 "hasn't",
 "won't",
 'wouldn',
 't',
 'ourselves',
 "should've",
 "don't",
 'doing',
 'can',
 'been',
 'into',
 'him',
 "it's",
 'a',
 'their',
 'had',
 'there',
 "shouldn't",
 'hers',
 'didn',
 "haven't",
 'after',
 'at',
 'ours',
 'were',
 'through',
 've',
 'some',
 'ma',
 'below',
 'again',
 "aren't",
 'on',
 'yourself',
 "that'll",
 'do',
 'between',
 "wasn't",
 "mightn't",
 'nor',
 'until',
 'itself',
 'has',
 'not',
 "mustn't",
 'myself',
 'the',
 'be',
 'was',
 'for',
 "couldn't",
 "you've",
 'mightn',
 'as',
 'same',
 'did',
 'than',
 'we',
 'its',
 'they',
 'are',
 's',
 "isn't",
 'couldn',
 'll',
 'will',
 'me',
 'when',
 'does',
 'how',
 'your',
 'won',
 'shouldn',
 'very',
 'this',
 "wouldn't",
 'over',
 'our',
 'by',
 'he',
 'now',
 'hasn',
 'o',
 'before',
 'from',
 'it',
 'what']

def sub_noisy_chars(token):
    return re.sub(r'\"', "", token)

def sub_string(token):
    return re.sub(r'[+-<>/?_\{\}~|`:;\'!@#$%^&*\(\)\[\]\\=®’\.\,]', "", token)

def split_by_slash(token):
    if type(token) != str:
        raise ValueError
    return re.sub('\/', ' ', token)

def remove_double_spaces(token):
    return re.sub(r' +', ' ', token)

def is_numeric(token):
    try:
        return isinstance(int(token), int)
    except:
        return False

def remove_quatation_marks(token):
    return re.sub('"', "", token)

def remove_stopwords(row):
    return ' '.join([token for token in row if token not in stop_words])


def transform(data):
    data.columns       = ['searchterm']
    data['searchterm'] = data['searchterm'].astype(str)
    data['searchterm'] = data['searchterm'].apply(lambda x: remove_stopwords(x.split()))
    data['searchterm'] = data['searchterm'].map(sub_string)
    data['searchterm'] = data['searchterm'].map(remove_quatation_marks)
    data['searchterm'] = data['searchterm'].map(split_by_slash)
    data['searchterm'] = data['searchterm'].map(remove_double_spaces)
    data['is_number']  = data['searchterm'].apply(is_numeric)
    data               = data[data['is_number'] == False].drop('is_number', axis=1).reset_index(drop=True)
    data               = data[data['searchterm'].apply(len) > 1].reset_index(drop=True)

    return set(data['searchterm'].unique())
